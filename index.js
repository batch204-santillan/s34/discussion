// require directive is uses to load the express module/packages.
// it allows us to access the method and functions in easily creating our server.
	const express = require("express");

// this creates an express application and stores this in a const called app
// In layman's term, app is our server.
	const app = express();
/*
	- setup for allowing the server to handle data from requests.
	- methods used express JS are middleware
		- Middleware is a software that provides commin services and capabiities to app outside of what's offered package.
*/	
	app.use(express.json()); // Allows your app to read json data (no need to use .parse and .stringify.

// For our application server to run , we need a port to listen to.
	const port = 3000;

/*		
	Express has methods corresponding to each HTTP method.
	"/" corresponds with our base URI
		- loclhost:3000/

	SYNTAX:
		app.httpMethod("/endpoint", (request, respoonse) => {})
*/
	app.get("/", (req,res) => {
	// res.send uses express JS module's method to send response back to the client.
	res.send ("Hello World");
});

/*
	CREATING A POST REQUEST
		- this route expects to receive a post request at th URI/endpoint "/hello"
*/
	app.post("/hello", (req, res) => {
		// req.body contains the contents/data of the request
		console.log (req.body);

		res.send (`Hello there ${req.body.firstName} ${req.body.lastName}!`);
	});


/*
	SCENARIO:

			We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:

*/
	//mock database
	let users = [
		{
			"username" : "janedoe",
			"password" : 'jane123'
		},
		{
			"username" : "johnsmith",
			"password" : 'john123'
		}
	];
	// CRUD OPERATIONS
	/*
	POST
		► this route expects to receive a POST request at the URI "/signup"	
		► this will creare a user object in the "users" array that mirrors a real-world registration
		► all fields are filled out 
	*/
	app.post("/signup" , (req, res) => {

		console.log (req.body);

		// if contents of "req.body" with property of "username" and "password" is nod empty
		if (req.body.username !== "" && req.body.password !== "" && req.body.username !== undefined && req.body.password !== undefined )
			{
				// to store the req.body sent via postman in the users array we will use the "push" method.
				// user object will be saved in our users array.
				users.push(req.body);

				res.send (`User ${req.body.username} is successfully registered!`);
			}
		// if username and password are not complete
		else 
			{
				res.send ("Please input BOTH username and password.")
			}
	});

	/*
	GET
		► This route expects to receive a GET request at the URI "/users" stores in the variable created above
	*/
		app.get("/users", (req, res) => {
			res.send(users);
		});

	/*
	UPDATE
		► this route expects to receive a PUT request at the URI "/change-password"
		► this will update the password of a user that matches the information provided in the client/postman
			► we will use the "username" as the search property
	*/
	app.put("/change-password", (req, res) => {

		// Creates a variable to store the message to be sent back to the client / Postman (response)
		let message;

		// create a for loop that will loop through the elements of the "users" array

		for(let i = 0; i<users.length ; i++) {

			// if the username provided in the client/Postman and the username of the current element/object in the loop is the same
			if (users[i].username === req.body.username) 
			{
				//changes the password of the user found by the loop into the password provided in the client
				users[i].password = req.body.password ;

				message = `User ${req.body.username}'s password has been updated.`

				//break out of the loop once the user matches the username provided in the client.
				break; 
			}
			// if username is not found
			else 
			{
				// changes the message to be sent back as the response
				message = "User does not exist!";
			}
		}
		// send a response back to the client once the password has been updated or if the user is not found
		res.send (message);

	});


	/*
		DELETE
			This route expects to receive a DELETE request at the URI "/delete-user"
			This will remove the user from the array for deletion
	*/

	app.delete("/delete-user", (req, res) => {

		// Creates a variable to store the message to be sent back to the client / Postman (response)
		let message;

		// create a for loop that will loop through the elements of the "users" array

		for(let i = 0; i<users.length ; i++) {

			// if the username provided in the client/Postman and the username of the current element/object in the loop is the same
			if (users[i].username === req.body.username) 
			{

				// splice method manipulates the array and removes the user object from the "users" array based on it's index
				users.splice(users[i], 1)

				message = `User ${req.body.username} has been deleted`

				//break out of the loop once the user matches the username provided in the client.
				break; 
			}
			// if username is not found
			else 
			{
				// changes the message to be sent back as the response
				message = "User does not exist!";
			}
		}
		// send a response back to the client once the password has been updated or if the user is not found
		res.send (message);

	});

// Returns a message to confirm that the server is running in the terminal
	app.listen(port, () => console.log (`Server is running at port: ${port}.`));

/*
	TO KILL A SERVER
		npx kill-port [port-number]
		eg: 
			npx kill-port 3000
*/

